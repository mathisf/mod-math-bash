#!/bin/bash

# NOM : script-langue-texte
# DESC : Script permettant de trouver une approximation du langage utilisé dans un texte
#        et de savoir quel sont les verbes les plus utilises pour le francais.
#        Ce script doit fonctionner avec n'importe quel langage si le fichier de
#        la langue correspondante est dans le dossier de langues
# NOTE : Le script supporte plusieurs parametres
#        Le dossier de langue par defaut est celui present dans le dossier du script
# EXAMPLE : $ ./script-langue-texte.sh ./textes/*

# Variable
dossierLangues="./langues"; # emplacement du dossier de langue qui contient les
                            # fichiers de langues

# Début
if [ ! -e "$1" ]; then # si il n'y a pas de fichier ou le fichier est inexistant en parametre alors
  echo "Il faut au moins un fichier texte existant en parametre";
else
  if [ ! -d ./resultat ]; then # si le fichier resultat n'existe pas
    mkdir -p ./resultat; # on creer le fichier resultat
  fi
  for fichiertexte in "$@"; do # pour chaque parametre faire
  ##############################################################################
  #                                                                            #
  #                        Nettoyage du fichier de texte                       #
  #                                                                            #
  ##############################################################################
  encoding=$(file -bi $fichiertexte | sed -e 's/.*[ ]charset=//' | awk '{print toupper($0)}'); # Detection de l'encoding
  #echo "L'encoding du fichier source est $encoding";
  iconv -f $encoding -t UTF-8 $fichiertexte -o $fichiertexte.a; # on convertit le fichier avec l'encoding trouve avant en UTF-8
  col -b < $fichiertexte.a > $fichiertexte.b; # remplacement des caracteres d'espaces speciaux par des espaces et des tabulations
  tr "	" "\012" < $fichiertexte.b > $fichiertexte.c; # on remplace les tabulations par des retours a la ligne
  tr " " "\012" < $fichiertexte.c > $fichiertexte.d; # on remplace les espaces par des retours a la ligne
  tr '[:upper:]' '[:lower:]' < $fichiertexte.d > $fichiertexte.e; # Remplacement des majuscules en minuscule
  sed -f filtre $fichiertexte.e > $fichiertexte.f; # Supression des caracteres non desirable
  fgrep -x -v -f stopwords $fichiertexte.f > $fichiertexte.g; # filtrage des stopwords
  awk 'length($0)>3' $fichiertexte.g > $fichiertexte.doublons; # Supression des mots de moins de 3 lettres
  sort $fichiertexte.doublons > $fichiertexte.h;  # tri
  uniq $fichiertexte.h $fichiertexte.i; # supression de doublon
  uniq -c $fichiertexte.h $fichiertexte.j; # Pour le resultat
  sort -bn -r $fichiertexte.j > $fichiertexte.mots; # Pour le resultat

  ##############################################################################
  #                                                                            #
  #                            Detection du langage                            #
  #                                                                            #
  ##############################################################################
  nbMax=0;
  langage="";
  for f in $dossierLangues/*.txt; do # pour chaque fichier de langue faire
    #echo "Traitement du fichier de langue : $f";
    nbTmp=$(fgrep -c -x -f $fichiertexte.i $f); # comparaison avec le fichier .i
    #echo "Nombre d'occurences entre les mots du textes et les mots de la langue : $nbTmp";
    if [ $nbTmp -gt $nbMax ]; then # si le nombre temporaire et superieur au nombre maximum alors
      nbMax=$nbTmp;
      langage=$f;
    fi
  done
  echo -e "Le fichier $fichiertexte correspond le plus au fichier de langue $langage avec $nbMax occurrences de mot";

  ##############################################################################
  #                                                                            #
  #                  Detection des verbes les plus utilisés (fr)               #
  #                                                                            #
  ##############################################################################
  if [[ $langage == *"fr"* ]]; then # si le nom du fichier de langue contient fr alors
    #echo "Traitement des verbes";
    fgrep -x -f touslesverbes.txt $fichiertexte.doublons > $fichiertexte.va; # on garde tout les verbes
    awk 'length($0)>4' $fichiertexte.va > $fichiertexte.vb; # on supprime tout les verbes de moins de 4 lettres
    sort $fichiertexte.vb > $fichiertexte.vc;
    uniq -c $fichiertexte.vc $fichiertexte.vd;
    sort -bn -r $fichiertexte.vd > $fichiertexte.verbes; # tri et comptage de chaque verbe
    echo "Les verbes les plus utilises du fichier $fichiertexte sont :";
    head -n 20 $fichiertexte.verbes # affiche les 20 premieres lignes du fichier de verbes
    cp $fichiertexte.verbes ./resultat/;
  fi
  cp $fichiertexte.mots ./resultat/;
  rm -f textes/*.txt.*; # suppression des fichiers temporaires
  done
fi
# fin
